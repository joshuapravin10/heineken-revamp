<?php

/*
* Add your own functions here. You can also copy some of the theme functions into this file. 
* Wordpress will use those functions instead of the original functions then.
*/

function load_child_theme_styles(){
    // If your css changes are minimal we recommend you to put them in the main style.css.
    // In this case uncomment bellow
    wp_enqueue_style( 'child-theme-style', get_stylesheet_directory_uri() . '/style.css' );
}

add_action('wp_enqueue_scripts', 'load_child_theme_styles', 999);
